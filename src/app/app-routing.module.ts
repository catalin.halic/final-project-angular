import { AdminGuard } from './admin/admin.guard';
import { AdminComponent } from './admin/admin.component';
import { UsersComponent } from './users/users.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './home/home.component';
import { ItemsComponent } from './items/items.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AdminGuard] },
  { path: 'auth', component: AuthComponent },
  { path: 'users', component: UsersComponent, canActivate: [AdminGuard] },
  { path: 'items', component: ItemsComponent, canActivate: [AdminGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
