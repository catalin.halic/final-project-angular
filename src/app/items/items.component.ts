import { ApiResponse } from './../utils/api-response.model';
import { ItemsService } from './items.service';
import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';
import { ItemModel } from './models/item.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
})
export class ItemsComponent implements OnInit {
  items: Array<ItemModel> = [];

  constructor(private itemsService: ItemsService) {}

  ngOnInit(): void {
    this.itemsService.getItems().subscribe((response) => {
      console.log(response);
      this.items = (response as ApiResponse).result;
    });
  }
}
