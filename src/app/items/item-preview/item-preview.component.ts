import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ItemModel } from '../models/item.model';

@Component({
  selector: 'app-item-preview',
  templateUrl: './item-preview.component.html',
  styleUrls: ['./item-preview.component.css'],
})
export class ItemPreviewComponent implements OnInit {
  @Input('item') item: ItemModel;
  @Output('edit') edit: EventEmitter<any> = new EventEmitter();
  @Output('delete') delete: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.item = {
      id: '0',
      title: `Title`,
      description: `Description`,
      imageURL: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
      itemType: 'car',
      price: '100',
      quantity: '0',
    };
  }

  ngOnInit(): void {}

  getItemTypeImage() {
    switch (this.item.itemType) {
      case 'car': {
        return 'https://material.angular.io/assets/img/examples/shiba2.jpg';
      }
      case 'bus': {
        return 'https://material.angular.io/assets/img/examples/shiba2.jpg';
      }

      default: {
        return 'https://material.angular.io/assets/img/examples/shiba2.jpg';
      }
    }
  }

  onEdit() {
    this.edit.emit('');
  }

  onDelete() {
    this.delete.emit('');
  }
}
