import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.css'],
})
export class UserPreviewComponent implements OnInit {
  @Input('user') user: UserModel;
  @Input('isAdminPage') isAdminPage: boolean = false;
  @Output('edit') edit: EventEmitter<any> = new EventEmitter();
  @Output('delete') delete: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.user = {
      id: '',
      firstName: '',
      lastName: '',
      username: '',
      password: '',
      email: '',
      phone: '',
      items: [],
    };
  }

  ngOnInit(): void {}

  onEdit(): void {
    this.edit.emit(this.user);
  }

  onDelete(): void {
    this.delete.emit(this.user);
  }
}
