import { ApiResponse } from './../utils/api-response.model';
import { ItemsService } from './../items/items.service';
import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';
import { ItemModel } from '../items/models/item.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  homePageInfo: HomePageInfo;
  items: Array<ItemModel> = [];

  constructor(
    private userService: UsersService,
    private itemsService: ItemsService
  ) {
    this.homePageInfo = {
      title: 'My app',
      description:
        'E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. Multe pachete de publicare pentru calculator şi editoare de pagini web folosesc acum Lorem Ipsum ca model standard de text, iar o cautare de "lorem ipsum" va rezulta în o mulţime de site-uri web în dezvoltare. Pe parcursul anilor, diferite versiuni au evoluat, uneori din intâmplare, uneori intenţionat (infiltrându-se elemente de umor sau altceva de acest gen).',
      author: 'Numele utilizatorului',
    };
  }

  ngOnInit(): void {
    this.itemsService.getItems().subscribe((response) => {
      console.log(response);
      this.items = (response as ApiResponse).result;
    });
  }
}

export interface HomePageInfo {
  title: string;
  description: string;
  author: string;
}
